import firebase from 'firebase/app'
import 'firebase/firestore' 
import 'firebase/auth'

const firebaseConfig = {
    apiKey: "AIzaSyBRu4tRQwbntxCfhf2KIHDwMCIxRg0bj-4",
    authDomain: "auth2020-11619.firebaseapp.com",
    databaseURL: "https://auth2020-11619.firebaseio.com",
    projectId: "auth2020-11619",
    storageBucket: "auth2020-11619.appspot.com",
    messagingSenderId: "659919070738",
    appId: "1:659919070738:web:69f054adfe0f4261b85acb"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

const db = firebase.firestore()
const auth = firebase.auth()

export{ db, auth}